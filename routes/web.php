<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|
|
|
|
 */

Route::get('/', [IndexController::class, 'index'])->name('index.home');
Route::get('/blog', [BlogController::class, 'index'])->name('blog.index');

Route::get('/{locale}', function ($locale) {
    Session::put('locale', $locale);

    return redirect()->back();
})->name('locale');
