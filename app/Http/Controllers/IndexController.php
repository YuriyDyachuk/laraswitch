<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {

        $data = [
            0 => [
                'name' => 'Yuriy',
                'age' => '30',
                'city' => 'Dnipro',
                'country' => 'Ukraine',
            ],
            1 => [
                'name' => 'John',
                'age' => '30',
                'city' => 'Dnipro',
                'country' => 'Ukraine',
            ],
            2 => [
                'name' => 'Mary',
                'age' => '30',
                'city' => 'Dnipro',
                'country' => 'Ukraine',
            ],
            3 => [
                'name' => 'Max',
                'age' => '30',
                'city' => 'Dnipro',
                'country' => 'Ukraine',
            ],
            4 => [
                'name' => 'Angela',
                'age' => '30',
                'city' => 'Dnipro',
                'country' => 'Ukraine',
            ]
        ];

        return view('index', [
            'data' => $data
        ]);
    }
}
