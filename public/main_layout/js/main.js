/*
|
|
|
|
 */

const progress = document.querySelector('.progressBar');

window.addEventListener('scroll', progressBar);

function progressBar(param) {
    let windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
    let windowHeight = document.documentElement.scrollHeight -document.documentElement.clientHeight;

    let per = (windowScroll / windowHeight) * 100;
    progress.style.width = per + '%'
}

$('#start').show();

setTimeout(() => {
    $(document).ready(function(){
        $.fn.animate_Text = function() {
            let string = this.text();

            return this.each(function(){
                let $this = $(this);
                $this.html(string.replace(/./g, '<span class="new">$&</span>'));

                $this.find('span.new').each(function(i, el){
                    setTimeout(function(){ $(el).addClass('div_opacity'); }, 35 * i);
                });
            });
        };

        $('#example').show();
        $('#example').animate_Text();
    });

}, 1500)

setTimeout(() => {
    $('#start').hide();
    $('#example').hide();
}, 25000)

