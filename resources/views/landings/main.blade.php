<div class="content-head"></div>

<!-- Animation text page -->
<div id="start"><h2>LET'S START TOGETHER</h2></div>
<pre id="example">
        setTimeout(() => {
            $(document).ready(function(){
                $.fn.animate_Text = function() {
                let string = this.text();

                return this.each(function(){
                    let $this = $(this);
                    $this.html(string.replace(/./g, '<span class="new">$&</span>'));
                    $this.find('span.new').each(function(i, el){
                        setTimeout(function(){ $(el).addClass('div_opacity'); }, 40 * i);
                    });
                });
            };
                $('#example').show();
                $('#example').animate_Text();
            });

        }, 10000)
    </pre>
<!-- Animation end text page -->
