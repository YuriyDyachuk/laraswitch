<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Css styles -->
    @include('layouts._includes.head')
    <style></style>
</head>
<body data-language="{{app()->getLocale()}}">
<div class="progressBar"></div>
    <section>
        <div id="app" class="wrap">
                <div class="aside" id="aside">
                    @include('layouts._includes.sidebar')
                </div>

                <div class="wrap-content">
                    @include('layouts._includes.top-menu')

                    <div class="content">
                        @yield('content')
                    </div>

                    @include('layouts._includes.footer')
                </div>
        </div>
    </section>

    @stack('tutorials')

    @yield('modals')

    @yield('scripts')

    @stack('scripts')
</body>
</html>
