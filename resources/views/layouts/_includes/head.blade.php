<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#ffffff">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

<!-- Favicon section -->
<link rel="icon" type="image/png" href="{{ url('/main_layout/images/favicons/favicon-32x32.png') }}" sizes="32x32">
<link rel="icon" type="image/png" href="{{ url('/main_layout/images/favicons/favicon-16x16.png') }}" sizes="16x16">
<link rel="manifest" href="{{ url('/main_layout/images/favicons/manifest.json') }}">
<link rel="mask-icon" href="{{ url('/main_layout/images/favicons/android-icon.svg') }}" color="#073a85">

<!-- Styles -->
@include('layouts._includes.css_files_new')

