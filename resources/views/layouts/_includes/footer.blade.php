<div class="footer" style="max-width: 100%;">

</div>

@section('scripts')
    {{--  Vue.js components  --}}
    <script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>

    {{-- Bower components js --}}
    <script type="text/javascript" src="{{ url('bower-components/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('bower-components/moment/min/moment-with-locales.min.js') }}"></script>
    @if(app()->isLocale('en'))
        <script type="text/javascript" src="{{ url('bower-components/moment/locale/en-gb.js') }}"></script>
    @else
        <script type="text/javascript" src="{{ url('bower-components/moment/locale/ru.js') }}"></script>
    @endif

    <script type="text/javascript" src="{{ url('bower-components/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('/main_layout/js/main.js') }}"></script>
@endsection
