
<nav class="aside__menu menu">
    <ul class="menu__list">
        <li class="menu__item">
            <a href="{{ route('index.home') }}" class="menu__link">
                <i class="fa fa-home menu__icon" aria-hidden="true"></i>
                <span class="menu__text">{{trans('all.home')}}</span>
            </a>
        </li>
        <li class="menu__item">
            <a href="#" class="menu__link">
                <i class="fa fa-user-circle" aria-hidden="true"></i>
                <span class="menu__text">{{trans('all.account')}}</span>
            </a>
        </li>
        <li class="menu__item">
            <a href="{{route('blog.index')}}" class="menu__link">
                <i class="fa fa-pencil-square" aria-hidden="true"></i>
                <span class="menu__text">{{trans('all.blogs')}}</span>
            </a>
        </li>
        <li class="menu__item">
            <a href="#" class="menu__link">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span class="menu__text">{{trans('all.maps')}}</span>
            </a>
        </li>
        <li class="menu__item">
            <a href="#" class="menu__link">
                <i class="fa fa-phone-square" aria-hidden="true"></i>
                <span class="menu__text">{{trans('all.address')}}</span>
            </a>
        </li>
    </ul>

    <div class="language">
        <ul>
            <li class="menu__item">
                <a class="menu__link active" id="lang" href="{{route('locale', ['locale' => 'en'])}}">EN</a>
            </li>
            <li class="menu__item">
                <a class="menu__link" id="lang" href="{{route('locale', ['locale' => 'ru'])}}">RU</a>
            </li>
        </ul>
    </div>
</nav>
