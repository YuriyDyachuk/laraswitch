<link rel="stylesheet" type="text/css" href="{{ url('bower-components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('bower-components/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('bower-components/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('bower-components/animate.css/animate.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">

<!--[ calendars styles ]-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
